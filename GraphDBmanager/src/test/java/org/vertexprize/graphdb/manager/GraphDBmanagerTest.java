/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.graphdb.manager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import static org.apache.tinkerpop.gremlin.process.traversal.P.inside;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Edge;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.apache.tinkerpop.gremlin.structure.io.graphson.GraphSONReader;
import org.apache.tinkerpop.gremlin.structure.io.graphson.GraphSONWriter;
import org.apache.tinkerpop.gremlin.tinkergraph.structure.TinkerGraph;
import org.testng.annotations.Test;

/**
 *
 * @author vaganovdv
 */
public class GraphDBmanagerTest {

    private Path resourceDirectory = Paths.get("src", "main", "resources", "graph");
    private final String graphFileName = "students.graphson";
    private final String graphPath = resourceDirectory.toFile().getAbsolutePath() + File.separator + File.separator + graphFileName;

    
    // Шаги по созданию графовой базы данных в памяти 
    
    
     // Вспомагатеьные классы для чтение и запись графа в файл в 
    private final   GraphSONWriter graphSONWriter = GraphSONWriter.build().create();
    private final   GraphSONReader graphSONReader = GraphSONReader.build().create();
    
    // Создание базы данных в памяти
    private final  TinkerGraph tinkerGraph = TinkerGraph.open();
    private final  GraphTraversalSource graphTraversalSource = tinkerGraph.traversal();
    
    
    // Файдл дя хранения базы данных 
    private final   File file = new File(graphPath);
    
    
    @Test(priority = 0, groups = {"graph-database"})
    public void createGraphDatabase() {

        // Создание узла для представления университета
        
       Vertex university = tinkerGraph.addVertex("Университет");             
       university.property("type", "Учебное заведение");
       university.property("name", "Тихоокеанский государсвенный университет");
       university.property("address", "г. Хабаровск, ул. Тихоокенская, 136");   
       university.property("год основания", 1958);   
       System.out.println("Создан узел Университет: id = "+university.id() +" label = "+university.label());
       
        // Создание узла для представления физического факультета
        Vertex phisical = tinkerGraph.addVertex("Подразделение");
        phisical.property("name", "Физический факультет");
        phisical.property("type", "Факультет");       
        System.out.println("Создан узел Факультет: id = "+phisical.id() +" label = "+phisical.label());
        
        // Создание узла для представления отдела кадров
        Vertex hr = tinkerGraph.addVertex("Подразделение");
        hr.property("name", "Отдел кадров");        
        System.out.println("Создан узел Отдел кадров: id = "+hr.id() +" label = "+hr.label());
        
         
        // Создание узла для представления химического факультета
        Vertex chemical = tinkerGraph.addVertex("Подразделение");
        chemical.property("name", "Химический факультет");
        chemical.property("type", "Факультет");
        System.out.println("Создан узел Факультет: id = "+chemical.id() +" label = "+chemical.label());
        
        
         // Добавление факультетов к университету
        Edge e1 = phisical.addEdge("явялется подразделением", university);
        Edge e2 = chemical.addEdge("явялется подразделением", university); 
        Edge e3 = hr.addEdge("явялется подразделением", university); 
        
        phisical.property("год основания", 1975);
        chemical.property("год основания", 1968);        
        System.out.println("Добавлена связь между: id = "+phisical.id()+ " и id = "+university.id() +" label = "+e1.label());
        System.out.println("Добавлена связь между: id = "+chemical.id()+ " и id = "+university.id() +" label = "+e2.label());
        
        
        // Создание узлов студенческих групп физического факультета       
        Vertex gPH1 = tinkerGraph.addVertex("Студенческая группа");
        gPH1.property("type", "Студенческая группа");     
        gPH1.property("name", "F1");  
        gPH1.property("studentCount", 15);  
        
        Vertex gPH2 = tinkerGraph.addVertex("Студенческая группа");
        gPH2.property("type", "Студенческая группа");     
        gPH2.property("name", "F2");
        gPH2.property("studentCount", 22);  
         
        Vertex gPH3 = tinkerGraph.addVertex("Студенческая группа");
        gPH3.property("type", "Студенческая группа");     
        gPH3.property("name", "F3");
        gPH3.property("studentCount", 20);  
        
        
        // Процесс добавления групп к физ. факультету
        gPH1.addEdge("группа факультета", phisical);
        gPH2.addEdge("группа факультета", phisical);
        gPH3.addEdge("группа факультета", phisical);
     
        
        // Формирование узлов студенческих групп химического факультета            
        Vertex gC1 = tinkerGraph.addVertex("Студенческая группа");
        gC1.property("name", "C1");
        gC1.property("studentCount", 35);  
        
             
        Vertex gC2 = tinkerGraph.addVertex("Студенческая группа");
        gC2.property("name", "C2");
        gC2.property("studentCount", 35);  
        
        Vertex gC3 = tinkerGraph.addVertex("Студенческая группа");
        gC3.property("name", "C3");
        gC3.property("studentCount", 15);  
        
        
        // Процесс добавления групп к хим. факультету
        gC1.addEdge("группа факультета", chemical);
        gC2.addEdge("группа факультета", chemical);
        gC3.addEdge("группа факультета", chemical);
     
        
        
        // Процесс добавления групп к хим. факультету
        gPH1.addEdge("слушает базовый курс ОБЩАЯ ФИЗИКА", phisical);
        gC1.addEdge("слушает базовый курс ОБЩАЯ ФИЗИКА", phisical);
        gPH1.addEdge("слушает базовый курс КОЛЛОИДНАЯ ХИМИЯ", chemical);
        
        
        // 
        System.out.println("Поисковые механизмы в графовых базах данных");
        
        // Подсчет вершин     
        Long vertexCount = university     // Взять вершину (university) 
                            .graph()      // Осуществить обход графа
                            .traversal()    
                            .V()          // Осуществить обход вершин  
                            .count()      // Обновить счетчик вершин   gVertexCount
                            .next();        
        System.out.println("Количество вершин в графе: "+vertexCount);
        
        
        // Подсчет ребер
        Long edgeCount = university     // Взять вершину (university) 
                            .graph()      // Осуществить обход графа
                            .traversal()    
                            .E()          // Осуществить обход ребер  
                            .count()      // Обновить счетчик ребер edgeCount
                            .next();
        
        System.out.println("Количество ребер в графе: "+edgeCount);
        
        
        List<Vertex> studGropsList1 =         
                university.graph()          // Взять вершину (university) 
                        .traversal()        // Осуществить обход графа
                        .V()                // Осуществить обход вершин                        
                        .hasLabel("Студенческая группа")    // Выбрать вершины с меткой Студенческая группа                       
                        .toList();                          // Преобразовать  в список
        
        System.out.println("Количество студенческих групп:"+studGropsList1.size() );
        
        
        // Поиск всех факультетов
        List<Vertex> facultyList =         
                university.graph()          // Взять вершину (university) 
                        .traversal()        // Осуществить обход графа
                        .V()                // Осуществить обход вершин                        
                        .hasLabel("Подразделение")    // Выбрать вершины с меткой Студенческая группа                       
                        .has("type", "Факультет") 
                        .toList();                          // Преобразовать  в список
        
        System.out.println("Список всех  факультетов университета:"+facultyList.size() );
        
        // Поиск отдела кадров
        List<Vertex> hrList =         
                university.graph()          // Взять вершину (university) 
                        .traversal()        // Осуществить обход графа
                        .V()                // Осуществить обход вершин                        
                        .hasLabel("Подразделение")    // Выбрать вершины 
                        .has("name", "Отдел кадров") 
                        .toList();                          // Преобразовать  в список
        
        System.out.println("Список отделов кадров университета:" + hrList.size() );
        
        // Сформировать список вершин   List<Vertex>  c именем studGropsList
        List<Vertex> studGropsList2 = 
                university.graph()          // Взять вершину (university) 
                        .traversal()        // Осуществить обход графа
                        .V()                // Осуществить обход вершин                        
                        .hasLabel("Студенческая группа")    // Выбрать вершины с меткой Студенческая группа
                        .has("studentCount", 35)             // Выбрать группы в которых число учащихся 35
                        .toList();                          // Преобразовать  в список
        
        System.out.println("Количество студенческих групп:"+studGropsList2.size());
       
        // Найти группу в которой от 20 до 22 студентов
        List<Vertex> toList = university.graph()          // Взять вершину (university) 
                .traversal()        // Осуществить обход графа
                .V()                // Осуществить обход вершин
                .hasLabel("Студенческая группа")            // Выбрать вершины с меткой Студенческая группа
                .has("studentCount", inside(19, 23))
                .toList();                      
        
         System.out.println("Количество студенческих групп в которых ровно от 20 до 22 студентов:"+toList.size());
    
        

        // Сохранение объекта в базе данных
        try {
            if (file.exists()) file.delete();
            
            file.createNewFile();
            OutputStream out = new FileOutputStream(file);

            graphSONWriter.writeGraph(out, tinkerGraph);
            System.out.println("Файл записан: " + file.getAbsolutePath());

        } catch (FileNotFoundException ex) {
            System.out.println("Ошибка создания файла базы данных [" + graphPath + "] не существует файл: ");
        } catch (IOException ex) {
            System.out.println("Ошибка создания экземпляров чтения и записи файлов - файл [" + graphPath + "] не существует");
        }
    }
    

}
