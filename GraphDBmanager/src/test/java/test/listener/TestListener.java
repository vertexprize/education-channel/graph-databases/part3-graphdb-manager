package test.listener;



import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

/**
 *
 * @author vaganovdv
 */
public class TestListener implements ITestListener {

    @Override
    public void onTestStart(ITestResult result) {
        System.out.println(String.format("%-20s %5s %s", "Test start", "==>", result.getMethod().getMethodName()));

    }

    @Override
    public void onTestSuccess(ITestResult result) {
        System.out.println(String.format("%-20s %5s %s", "Test success", "==>", result.getMethod().getMethodName()));
    }

    @Override
    public void onTestFailure(ITestResult result) {
        System.out.println(String.format("%-20s %5s %s", "Test failure", "==>", result.getMethod().getMethodName()));
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        System.out.println(String.format("%-20s %5s %s", "Test skipped", "==>", result.getMethod().getMethodName()));
    }

    @Override
    public void onStart(ITestContext context) {
        System.out.println(String.format("\n%-20s %5s %s", "Start tests", "==>", context.getName()));
    }

    @Override
    public void onFinish(ITestContext context) {
        System.out.println("\n----------------------- Test Report --------------------------");
        System.out.println(String.format("%-20s %s", "Total  Passed:", "[" + context.getPassedTests().size() + "]"));
        if (context.getFailedTests().size() > 0) {
            System.out.println(String.format("%-20s %s", "Total  Failed:", "[" + context.getFailedTests().size() + "] <=== !"));
        } else {
            System.out.println(String.format("%-20s %s", "Total  Failed:", "[" + context.getFailedTests().size() + "]"));
        }
        System.out.println(String.format("%-20s %s", "Total Skipped:", "[" + context.getSkippedTests().size() + "]"));
        System.out.println("--------------------------------------------------------------\n");
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

}
